<?php
include 'db.php';
$url = "https://v2.rki.marlon-lueckert.de/germany";
$json = file_get_contents($url);
$json_data = json_decode($json, true);
$updated_date_on_gr = $json_data["meta"]["lastUpdate"];


$url2 = "https://v2.rki.marlon-lueckert.de/states";
$json2 = file_get_contents($url2);
$json_data2 = json_decode($json2, true);
$updated_date_on_st = $json_data["meta"]["lastUpdate"];


$handle = curl_init($url);
curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
$response = curl_exec($handle);
$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

$handle2 = curl_init($url2);
curl_setopt($handle2,  CURLOPT_RETURNTRANSFER, TRUE);
$response2 = curl_exec($handle2);
$httpCode2 = curl_getinfo($handle2, CURLINFO_HTTP_CODE);


$germany_check = 0;
$state_check = 0;
$final_data = array();
$get1 =mysqli_query($conn,"select * from corona_data order by id desc limit 1");
$count1 = mysqli_num_rows($get1);
$row1;
if($count1>0)
{
    $row1 = mysqli_fetch_assoc($get);
}
if($httpCode == 404 && $httpCode2 == 404) {
    // $germany_check =1;
    json_response(400,"Sorry no response from api server for germany and state data",null);
}
else if($httpCode != 404 && $httpCode2 == 404)
{
    if($row1['state_data']=="")
    {
        $final_data[] = array(
                "germany_data_status" => "1",
                "germany_data" => $json_data,
                "state_data_status" => "0",
                "state_data" => "null"
            );
    }
    else
    {
        $json_data5 = json_decode($row1['state_data'],true);
        $final_data[] = array(
                "germany_data_status" => "1",
                "germany_data" => $json_data,
                "state_data_status" => "1",
                "state_data" => $json_data5
            );
    }        
    json_response(401,"Germany data attached, state data server not responding",$final_data);
}
else if($httpCode == 404 && $httpCode2 != 404)
{
    if($row1['germany_data']=="")
    {
        $final_data[] = array(
                "germany_data_status" => "0",
                "germany_data" => "null",
                "state_data_status" => "1",
                "state_data" => $json_data2
            );
    }
    else
    {
        $json_data6 = json_decode($row1['germany_data'],true);
        $final_data[] = array(
                "germany_data_status" => "1",
                "germany_data" => $json_data6,
                "state_data_status" => "1",
                "state_data" => $json_data2
            );
    }
     json_response(402,"State data attached, Germany data server not responding",$final_data);
    
    
}
else
{
     $final_data[] = array(
                "germany_data_status" => "1",
                "germany_data" => $json_data,
                "state_data_status" => "1",
                "state_data" => $json_data2
            );
     json_response(100,"Germany data and server data attached",$final_data);
}

curl_close($handle);
curl_close($handle2);

$get =mysqli_query($conn,"select * from corona_data order by id desc limit 1");
$count = mysqli_num_rows($get);
if($count>0)
{
    $gr_q=0;
    $st_q=0;
    $row = mysqli_fetch_assoc($get);
    $germany_data = $row['germany_data'];
    
    $json_data3 = json_decode($germany_data, true);
    $updated_date_db_gr= $json_data3["meta"]["lastUpdate"];
    
    $state_data = $row['state_data'];
     $json_data4 = json_decode($state_data, true);
    // print_r($json_data4);
    $updated_date_db_st= $json_data4["meta"]["lastUpdate"];
    // $st_compare = var_dump($updated_date_db_st == $updated_date_on_st);
    // $gr_compare = var_dump($updated_date_db_gr == $updated_date_on_gr);
    
    // if($gr_compare==true)
    // {
        // echo $updated_date_db_gr;
        // echo "<br>";
        // echo $updated_date_on_gr;
        
    // }
    if($updated_date_on_gr==$updated_date_db_gr)
    {
        $gr_q=1;
    }
    if($updated_date_on_st==$updated_date_db_st)
    {
        $st_q=1;
    }
    
    if($gr_q==1 && $st_q==1)
    {
    }
    else if($gr_q==0 && $st_q==1)
    {
     
        $update = mysqli_query($conn,"update corona_data set germany_data='".$json."', state_data='".$state_data."' where 1");
    }
    else if($gr_q==1 && $st_q==0)
    {
     
        $update = mysqli_query($conn,"update corona_data set state_data='".$json2."', germany_data='".$germany_data."' where 1");
    }
    else
    {
     
        if($httpCode != 404 && $httpCode2 != 404)
        {
        $update = mysqli_query($conn,"update corona_data set germany_data='".$json."', state_data='".$json2."' where 1");
        }
    }
    
}
else
{
$insert = mysqli_query($conn,"insert into corona_data (germany_data,state_data) values('".$json."', '".$json2."')");    
}



function json_response($a,$b,$c)
{
    $json['status']=$a;
    $json['message']=$b;
    $json['data']=$c;
    echo json_encode($json);
}
?>