import * as React from "react";
import {Button, List, ListItem, Modal} from 'semantic-ui-react';
import {IfBox} from "../style/if";

export interface InstructionsProps {
    use_bot?: boolean,
    onClose: () => void,
}

export interface InstructionsState {
}


export class Instructions extends React.Component<InstructionsProps, InstructionsState> {
    use_bot = false;

    constructor(props: any) {
        super(props)
        if (this.props.use_bot) {
            this.use_bot = this.props.use_bot;
        }
        this.state = {}
    }

    render() {
        return <Modal dimmer open>
            <Modal.Header>Instructions</Modal.Header>
            <Modal.Content>
                <Modal.Description>
                    <List bulleted>
                        <IfBox shouldShow={this.props.use_bot}>
                            <ListItem>Type the input field to communicate with the bot.
                                The bot will reply you based on its understanding of your message.</ListItem>
                            <ListItem>If the bot understands that you want to search for something.
                                It will respond with search results from Google.(example: Search German alphabets)</ListItem>
                            <ListItem>You can check out the results in detail by clicking them to open in a new
                                tab.</ListItem>
                            <ListItem>You can either ask the bot for next batch of results from Google for the same
                                search
                                or tell the bot that you are satisfied with the results already shown.</ListItem>
                            <ListItem>The Feedback Modal will popup when you click any of the results or if you are
                                satisfied with the results shown by the bot.</ListItem>
                        </IfBox>
                        <IfBox shouldShow={!this.props.use_bot}>
                            <ListItem>Type the Search in the Search Field.</ListItem>
                            <ListItem>Select results from the Result list to open them in a new Tab.</ListItem>
                            <ListItem>The Feedback Modal will show popup.</ListItem>
                        </IfBox>
                        <br/>Please provide us with your valuable feedback.
                        <br/>Thank you!
                    </List>
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button positive icon='x' labelPosition='right' content="Close"
                        onClick={() => {
                            this.props.onClose();
                        }}/>
            </Modal.Actions>
        </Modal>
    }
}