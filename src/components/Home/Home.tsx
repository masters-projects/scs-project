import * as React from "react";
import {
    Dimmer,
    Divider,
    Grid,
    Header, Image, Label, Loader, Message, Rail,
    Segment,
    Statistic
} from "semantic-ui-react";
import {Map} from "../Map/Map";
import ReactTooltip from "react-tooltip";
import {ApiResponse, result, requestData} from "../sendRequest";
import {germany} from "../Map/germany.json";
import {IfBox} from "../style/if";
import moment from "moment/moment";
import img1 from './img/img1.png'
import img2 from './img/img2.png'
import img3 from './img/img3.png'
import img4 from './img/img4.jpeg'
import img5 from './img/img5.jpeg'
import img6 from './img/img6.jpeg'
import img7 from './img/img7.jpeg'

type HomeState = {
    apiSuccess: boolean,
    showNotification: boolean,
    update: boolean,
    loading: boolean,
    tooltipContent: result,
    selectedState: result,
    notification: string,
    statistics: {
        state: {
            zones: {
                redZone: any[],
                greenZone: any[]
            },
            data: any
        },
        country: result
    }
}

export class Home extends React.Component<any, HomeState> {
    map: any = germany

    constructor(props: any) {
        super(props)
        this.state = {
            apiSuccess: null,
            showNotification: null,
            update: true,
            loading: true,
            tooltipContent: {},
            selectedState: {},
            notification: "",
            statistics: {
                state: {
                    zones: {
                        redZone: [],
                        greenZone: []
                    },
                    data: {}
                },
                country: {}
            }
        }
    }

    async componentDidMount() {
        try {
            const results: ApiResponse = await requestData();
            const countryResults = results.country;
            const stateResults = results.state;
            // const stateResults = await sendStateRequest();
            // const stateUpdateDate = moment(stateResults.response.meta?.lastUpdate);
            // const countryUpdateDate = moment(countryResults?.lastUpdate);
            // console.log(stateResults)
            const data = stateResults.response
            const redZone = []
            const greenZone = []
            if (data) {
                Object.keys(data).forEach((stateName: any) => {
                    const state = data[stateName]
                    if (state.weekIncidence >= 90) {
                        redZone.push("de-" + state.abbreviation.toLowerCase())
                    } else {
                        greenZone.push("de-" + state.abbreviation.toLowerCase())
                    }
                })
                // console.log("zonedata: ", this.state.statistics.state.zones)
            }

            this.setState({
                statistics: {
                    state: {
                        zones: {
                            redZone: redZone,
                            greenZone: greenZone
                        },
                        data: stateResults,
                    },
                    country: countryResults
                },
                apiSuccess: true,
                showNotification: true,
                notification: "Data successfully updated!"
            }, () => {
                setTimeout(() => {
                    this.setState({
                        showNotification: false,
                        loading: false,
                    })
                }, 2000)
            });
        } catch (e) {
            console.log(e)
            this.setState({
                apiSuccess: false,
                notification: "unfortunately, we couldn't retrieve any updated data."
            });

        }

    }

    render() {

        return <Segment style={{overflow: "auto", height: "99vh", top: "5px", width: "99%", left: "7px"}}>
            <Dimmer.Dimmable blurring dimmed={this.state.loading}>
                <Dimmer active={this.state.loading}>
                    <IfBox shouldShow={this.state.apiSuccess === null}>
                        <Loader>Loading</Loader>
                    </IfBox>
                    <IfBox shouldShow={this.state.apiSuccess != null}>
                        {this.state.apiSuccess ?
                            <Message positive>
                                <Message.Header>{this.state.notification}</Message.Header>
                            </Message>
                            :
                            <Message negative>
                                <Message.Header>{this.state.notification}</Message.Header>
                            </Message>

                        }
                    </IfBox>
                </Dimmer>
                <ReactTooltip>
                    <span style={{fontWeight: "bold"}}>{this.state.tooltipContent.name + ": "}</span>
                    <br/>
                    {"cases: " + this.formatNumbers(this.state.tooltipContent.cases)}
                    <br/>
                    {"casesPer100k: " + this.formatNumbers(this.state.tooltipContent.casesPer100k)}
                    <br/>
                    {"casesPerWeek: " + this.formatNumbers(this.state.tooltipContent.casesPerWeek)}
                    <br/>
                    {"deaths: " + this.formatNumbers(this.state.tooltipContent.deaths)}
                    <br/>
                    {"deathsPerWeek: " + this.formatNumbers(this.state.tooltipContent.deathsPerWeek)}
                    <br/>
                    <br/>
                    <span style={{color: "lightblue"}}>Click for more information.</span>
                </ReactTooltip>
                <IfBox shouldShow={this.state.showNotification}>
                    {this.state.apiSuccess != null && this.state.apiSuccess ?
                        <Rail internal position='right' style={{zIndex: '999', top: '13px', right: '-42px'}}>
                            <Message positive>
                                <Message.Header>{this.state.notification}</Message.Header>
                            </Message>
                        </Rail>
                        : <Rail internal position='right' style={{zIndex: '999', top: '13px', right: '-42px'}}>
                            <Message negative>
                                <Message.Header>{this.state.notification}</Message.Header>
                            </Message>
                        </Rail>
                    }
                </IfBox>
                <Grid columns={2}>
                    <Grid.Row divided>
                        <Grid.Column width={7}>
                            {this.displayCountryStats()}
                            <Map map={this.map}
                                 setTooltipContent={this.updateTooltip}
                                 clicked={this.updateSelectedState}
                                 stateZones={this.state.statistics.state.zones}/>
                            {this.displayStateStats()}

                        </Grid.Column>
                        <Grid.Column width={9}>
                            {this.displayPredictions()}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                {/*<IfBox shouldShow={this.state.showInstructions}>*/}
                {/*    <Instructions onClose={() => {this.setState({showInstructions: false})}} use_bot={!this.state.showFieldOnly}/>*/}
                {/*</IfBox>*/}
            </Dimmer.Dimmable>
        </Segment>
    }

    displayPredictions = () => {

        return <div>
            <Header as='h2' floated='left'>
                Predictions using SIR model:
            </Header>
            <Header as='h5' floated='right'>
                Source: Robert Koch-Institut
                <br/>
                Used dataset: {moment('02.03.2021 01:00').format("LLLL")}
            </Header>
            <Divider hidden/>
            <Divider hidden/>
            <Divider hidden/>
            <Divider />
            <div style={{width: "90%"}}>
                <Statistic.Group size={"mini"}>
                    <Statistic>
                        <Statistic.Value>2237790</Statistic.Value>
                        <Statistic.Label>Infected:</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>1973150</Statistic.Value>
                        <Statistic.Label>Recovered:</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>205684</Statistic.Value>
                        <Statistic.Label>Active cases:</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>58956</Statistic.Value>
                        <Statistic.Label>Deaths:</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>0.212</Statistic.Value>
                        <Statistic.Label>Infection rate (A):</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>0.151</Statistic.Value>
                        <Statistic.Label>Recovery rate (B):</Statistic.Label>
                    </Statistic>

                </Statistic.Group>

                <div style={{paddingTop: "10px"}}>
                    <span style={{fontWeight: "bold"}}>Initial infected number = Io =</span> 5*(1e-2)
                </div>
            </div>

            <div>
                <Grid>
                    <Grid.Row textAlign={"center"}>
                        <Grid.Column width={7}>
                            <Image src={img2} size='large'/>
                            <Label>Normal SIR model</Label>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Image src={img3} size='large'/>
                            <Label>SIR model with vaccine</Label>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <div style={{paddingTop: "10px", paddingLeft: "10px"}}>
                            <span style={{fontWeight: "bold", textDecoration: "underline", fontSize: 'large'}}>
                                Prediction for the next 70 weeks is shown in the graphs:
                            </span>
                        </div>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column width={5}>
                            <Image src={img4} size='medium'/>
                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={img6} size='medium'/>
                        </Grid.Column>
                        <Grid.Column width={5}>
                            <Image src={img5} size='medium'/>
                        </Grid.Column>  </Grid.Row>
                    <Grid.Row>

                        <Grid.Column width={8}>
                            <Image src={img7} size='medium'/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </div>
    }
    displayCountryStats = () => {
        const countryUpdateDate = moment(this.state.statistics.country?.lastUpdate);

        return <div>
            <Header as='h5' floated='right' >
                Source: {this.state.statistics?.country?.source}
                <br/>
                Last Update: {" " + countryUpdateDate.format("LLLL")}
            </Header>
            <Header as='h2' floated='left'>
                Germany:

            </Header>
            <Divider hidden/>
            <Divider hidden/>
            <Divider hidden/>
            <Divider/>
            <Statistic.Group size={"mini"}>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.statistics?.country?.cases) ?? 0}</Statistic.Value>
                    <Statistic.Label>Cases</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.statistics?.country?.casesPer100k) ?? 0}</Statistic.Value>
                    <Statistic.Label>Cases per 100k</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.statistics?.country?.casesPerWeek) ?? 0}</Statistic.Value>
                    <Statistic.Label>Cases per week</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.statistics?.country?.deaths) ?? 0}</Statistic.Value>
                    <Statistic.Label>Deaths</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.statistics?.country?.recovered) ?? 0}</Statistic.Value>
                    <Statistic.Label>Recovered</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.statistics?.country?.weekIncidence) ?? 0}</Statistic.Value>
                    <Statistic.Label>Week Incidence</Statistic.Label>
                </Statistic>
                {/*<Statistic>*/}
                {/*    <Statistic.Value>{this.state.statistics?.country?.deathsPerWeek}</Statistic.Value>*/}
                {/*    <Statistic.Label>Deaths per week</Statistic.Label>*/}
                {/*</Statistic>*/}

            </Statistic.Group>


        </div>

    }
    displayStateStats = () => {
        const stateUpdateDate = moment(this.state.selectedState?.lastUpdate);
        return <div><Header as='h5' floated='right'>
            Source: {this.state.statistics?.state.data.source}
            <br/>
            Last Update: {" " + stateUpdateDate.format("LLLL")}
        </Header>
            <Header as='h2' floated='left'>
                {this.state.selectedState?.name ? this.state.selectedState?.name + ":" : "Please Select a state"}
            </Header>
            <Divider hidden/>
            <Divider hidden/>
            <Divider hidden/>
            <Divider/>

            <Statistic.Group size={"mini"}>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.selectedState?.cases ?? 0)}</Statistic.Value>
                    <Statistic.Label>Cases</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.selectedState?.casesPer100k ?? 0)}</Statistic.Value>
                    <Statistic.Label>Cases per 100k</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.selectedState?.casesPerWeek ?? 0)}</Statistic.Value>
                    <Statistic.Label>Cases per week</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.selectedState?.deaths ?? 0)}</Statistic.Value>
                    <Statistic.Label>Deaths</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{this.formatNumbers(this.state.selectedState?.deathsPerWeek ?? 0)}</Statistic.Value>
                    <Statistic.Label>Deaths per week</Statistic.Label>
                </Statistic>

            </Statistic.Group>
        </div>
    }
    updateTooltip = (contentUpdate) => {
        // console.log("STATE :",this.state.statistics.state)
        if (this.state.statistics.state.data.success && this.state.statistics.state.data.response[contentUpdate.split("-")[1].toUpperCase()]) {
            const data = this.state.statistics.state.data.response[contentUpdate.split("-")[1].toUpperCase()]
            // console.log("UPDATE", data)
            const formattedData: result = {
                name: data.name,
                cases: data.cases,
                casesPer100k: data.casesPer100k,
                casesPerWeek: data.casesPerWeek,
                deaths: data.deaths,
                deathsPerWeek: data.deathsPerWeek
            }
            this.setState({tooltipContent: formattedData})
        } else {
            console.log("error : key not matched")
        }
    }
    updateSelectedState = (state) => {
        if (this.state.statistics.state.data.success && this.state.statistics.state.data.response[state.split("-")[1].toUpperCase()]) {
            const data = this.state.statistics.state.data.response[state.split("-")[1].toUpperCase()]
            // console.log("UPDATE", data)
            const formattedData: result = {
                success: true,
                name: data.name,
                cases: data.cases,
                casesPer100k: data.casesPer100k,
                casesPerWeek: data.casesPerWeek,
                deaths: data.deaths,
                deathsPerWeek: data.deathsPerWeek
            }
            this.setState({selectedState: formattedData})
        } else {
            this.setState({selectedState: {success: false}})
            console.log("error : state not matched")
        }
    }


    formatNumbers(number) {
        return number?.toLocaleString(undefined, {maximumFractionDigits: 2})
    }

    private update() {
        this.setState((prevState) => ({
            update: !prevState.update
        }));
    }
}