export interface ApiResponse {
    country: result,
    state: {
        success: boolean,
        response: any,
        source: string,
        error?: string,
    },
}

// export interface ApiSearchStats {
//     formattedSearchTime?: string
//     formattedTotalResults?: string
//     searchTime?: number
//     totalResults?: number
// }

export interface result {
    error?: string
    success?: boolean
    name?: string
    lastUpdate?: Date
    cases?: number
    casesPer100k?: number
    recovered?: number
    casesPerWeek?: number
    deaths?: number
    deathsPerWeek?: number
    weekIncidence?: number
    source?: string
}

export async function requestData(): Promise<ApiResponse> {
    const resultToSend: ApiResponse = {country: {}, state: {response: null, source: "", success: false}}
    const jhDataSet = "https://corona-api.com/countries/DE"
    const rkGermanyDataSet = "https://www.rebootera.com/akt/api.php"

    await fetch(rkGermanyDataSet)
        .then(response => response.json())
        .then(response => {
            const country_result = response.data[0].germany_data;
            const state_result = response.data[0].state_data;
            console.log("country_result:", country_result)
            console.log("state_result:", state_result)
            if (response.error) {
                response.country.success = false

            } else {
                if (!+response.data[0].germany_data_status) {
                    resultToSend.country.error = " Api for Data on Germany failed, Data retrieved from database."
                }
                if (country_result) {
                    resultToSend.country.success = true
                    resultToSend.country.lastUpdate = country_result?.meta?.lastUpdate
                    resultToSend.country.cases = country_result.cases
                    resultToSend.country.casesPer100k = country_result.casesPer100k
                    resultToSend.country.casesPerWeek = country_result.casesPerWeek
                    resultToSend.country.recovered = country_result.recovered
                    resultToSend.country.deaths = country_result.deaths
                    resultToSend.country.deathsPerWeek = null
                    resultToSend.country.weekIncidence = country_result.weekIncidence
                    resultToSend.country.source = country_result?.meta?.source
                }
                if (!+response.data[0].germany_data_status) {
                    resultToSend.state.error = " Api for Data on Germany's States failed, Data retrieved from database."
                }
                if (state_result) {
                    resultToSend.state.success = true
                    resultToSend.state.response = state_result.data
                    resultToSend.state.source = state_result?.meta?.source
                }
            }
        });
    // await fetch(jhDataSet)
    //             .then(response => response.json())
    //             .then(response => {
    //                 console.log("API Result: ", response.data)
    //                 if (response.error) {
    //                     console.log("API Error: ", response.error)
    //                     countryResponse.success = false
    //
    //                 }else{
    //                     console.log("API Result: ", response)
    //                     countryResponse.success = true
    //                     countryResponse.lastUpdate = response?.meta?.lastUpdate
    //                     countryResponse.cases = response.cases
    //                     countryResponse.casesPer100k = response.casesPer100k
    //                     countryResponse.casesPerWeek = response.casesPerWeek
    //                     countryResponse.deaths = response.deaths
    //                     countryResponse.deathsPerWeek = null
    //                 }
    //             });
    return resultToSend

}

// export async function sendStateRequest(): Promise<any> {
//     let colors =[]
//     const mapColors = "https://api.corona-zahlen.org/map/states/legend"
//
//     await fetch(mapColors)
//         .then(response => response.json())
//         .then(response => {
//             if (response.error) {
//                 console.log("API Error: ", response.error)
//
//             } else {
//                 // console.log("API Result: ", response)
//                 stateResponse.success = true
//             }
//         });
//     const rkStateDataSet = "https://v2.rki.marlon-lueckert.de/states"
//
//     await fetch(rkStateDataSet)
//         .then(response => response.json())
//         .then(response => {
//             if (response.error) {
//                 console.log("State API Error: ", response.error)
//
//             } else {
//                 // console.log("API Result: ", response)
//                 stateResponse.success = true
//                 stateResponse.response = response
//                 stateResponse.source = response?.meta?.source
//
//             }
//         });
//
//     return stateResponse
// }