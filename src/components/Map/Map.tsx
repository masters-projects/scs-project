import * as React from "react";
import {VectorMap} from '@south-paw/react-vector-maps';
import './Map.scss';
import {Container, Grid} from "semantic-ui-react";

type MapProps = {
    map: JSON
    stateZones: {
        redZone: any[],
        greenZone: any[]
    }
    setTooltipContent: (content) => void
    clicked: (state) => void
}

export class Map extends React.Component<MapProps, any> {

    constructor(props: any) {
        super(props)
        this.state = {
            hovered: null,
            focused: null,
            clicked: null,
            update: true,
            content: "",
            redZone: [],
            greenZone: [],
        }
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps: Readonly<MapProps>, prevState: Readonly<any>, snapshot?: any) {
        if (this.state.redZone !== this.props.stateZones.redZone || this.state.greenZone !== this.props.stateZones.greenZone) {
            // console.log("LIST STATE",this.state)
            this.setState({redZone: this.props.stateZones.redZone, greenZone: this.props.stateZones.greenZone})
            this.forceUpdate()
        }
    }

    render() {
        const layerProps = {
            onMouseEnter: ({target}) => {
                // console.log("TARGET: ", target)
                this.setState({hovered: target.attributes.name.value, content: target.attributes.name.value});
                this.props.setTooltipContent(target.attributes.id.value);
            },
            onMouseLeave: ({target}) => this.setState({hovered: 'None', content: ""}),
            onFocus: ({target}) => this.setState({focused: target.attributes.name.value}),
            onBlur: ({target}) => this.setState({focused: 'None'}),
            onClick: ({target}) => {
                this.setState({clicked: target.attributes.id.value});
                this.props.clicked(target.attributes.id.value);
            }
        }
        return <div>
            <div id="map" data-tip="">
                <VectorMap {...this.props.map} checkedLayers={this.props.stateZones.redZone}
                           currentLayers={this.props.stateZones.greenZone} layerProps={layerProps}/>
                {/*<hr/>*/}
                {/*<p>Hovered: {this.state.hovered && <code>{this.state.hovered}</code>}</p>*/}
                {/*<p>Focused: {this.state.focused && <code>{this.state.focused}</code>}</p>*/}
                {/*<p>Clicked: {this.state.clicked && <code>{this.state.clicked}</code>}</p>*/}
            </div>
            <div>
                <div style={{
                    background: "#132747c4", width: "5px", height: "5px", margin: "5px",
                    display: "inline-block"
                }}/>
                Weekly incidence {'<'} 50
                <br/>
                <div style={{
                    background: "#AB1316", width: "5px", height: "5px", margin: "5px",
                    display: "inline-block"
                }}/>
                Weekly incidence {'>='} 50
            </div>
        </div>
    }
}